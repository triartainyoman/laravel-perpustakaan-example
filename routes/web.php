<?php

use App\Http\Controllers\PerpustakaanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Laravel 5 - 7
Route::get('/buku', 'PerpustakaanController@index');
Route::get('buku/create', 'PerpustakaanController@create');
Route::get('/buku/edit', 'PerpustakaanController@edit');
    

// Laravel 8
// Route::get('/buku', [PerpustakaanController::class, 'index']);
