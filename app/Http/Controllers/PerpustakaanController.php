<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PerpustakaanController extends Controller
{
    public function index()
    {
        return view('buku.index');
    }

    public function create()
    {
        return view('buku.create');
    }

    public function edit()
    {
        return view('buku.edit');
    }
}
