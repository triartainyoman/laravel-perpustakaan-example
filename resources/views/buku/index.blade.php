@extends('partials.main')

@section('content')
  <h1>Tabel Buku</h1>

  <a href="/buku/create" type="button" class="btn btn-primary">Tambah Buku</a>

  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Buku</th>
        <th scope="col">Penulis</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
        <td>
          <a href="/buku/edit" type="button" class="btn btn-success">Edit</a>
          <button type="button" class="btn btn-danger">Delete</button>
        </td>
      </tr>
      <tr>
        <th scope="row">2</th>
        <td>Jacob</td>
        <td>Thornton</td>
        <td>
          <a href="/buku/edit" type="button" class="btn btn-success">Edit</a>
          <button type="button" class="btn btn-danger">Delete</button>
        </td>
      </tr>
      <tr>
        <th scope="row">3</th>
        <td colspan="2">Larry the Bird</td>
        <td>
          <a href="/buku/edit" type="button" class="btn btn-success">Edit</a>
          <button type="button" class="btn btn-danger">Delete</button>
        </td>
      </tr>
    </tbody>
  </table>
@endsection