@extends('partials.main')

@section('content')
  <h1>Create Buku</h1>

  <form>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Judul Buku</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="mb-3">
      <label for="exampleInputPassword1" class="form-label">Pengarang</label>
      <input type="text" class="form-control" id="exampleInputPassword1">
    </div>
    <button type="submit" class="btn btn-primary">Tambahkan Buku</button>
  </form>
@endsection